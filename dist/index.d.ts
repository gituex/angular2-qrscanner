import { ModuleWithProviders } from '@angular/core';
export * from './component/qrscanner.component';
export declare class QrScannerModule {
    static forRoot(): ModuleWithProviders;
}
